<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Student extends Model
{
    use HasFactory;
    protected $guarded=[];

    public function scopeList($query){
        $query->select('students.id','person','people.identification as identification','people.name as name','people.lastname as lastname',
        'people.birthdate as birthdate','people.main_phone as main_phone','people.sec_phone as sec_phone',//'students.interest_courses as interests',
        'id_types.name as id_type','enrolment_statuses.name as status','people.email as email')
        ->join('people','people.id','=','students.person')
        ->join('id_types','id_types.id','=','people.id_type')
        ->join('enrolment_statuses','enrolment_statuses.id','=','students.enrolment_status')
        ;

    }
    public function scopeDetails($query){
        $query->select('students.id','people.identification as identification',
        'people.name as name','people.lastname as lastname','people.main_phone as main_phone',
        'people.sec_phone as sec_phone','id_types.name as id_type','id_types.id as id_type_id',
        'people.birthdate as birthdate','people.email as email','attendant',
        
        'people2.id_type as id_type_id_a','people2.identification as identification_a','people2.name as name_a',
        'people2.lastname as lastname_a','people2.main_phone as main_phone_a',
        'people2.email as email_a',
        )
        ->join('people','people.id','=','students.person')
        ->join('people as people2','people2.id','=','students.attendant')
        ->join('id_types','id_types.id','=','people.id_type')
        ->join('id_types as id_types2','id_types2.id','=','people2.id_type')
        ;

       
    }
    

    public function interest_courses()
    {
        return $this->hasMany('App\Models\InterestCoursesStudent');
    }
}
