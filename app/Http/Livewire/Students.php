<?php

namespace App\Http\Livewire;
use Illuminate\Support\Facades\Auth;

use Livewire\Component;
use livewire\withPagination;

use App\Models\Student;
use App\Models\Person;
use App\Models\IdType;
use App\Models\InterestCoursesStudent;
use App\Models\CourseCategory;
use DateTime;


class Students extends Component
{
    public $student, $attendant,$id_type, $course_categories, $interests_by_student;
    public $title, $selected_id, $editionMode, $age;
    
    public function mount(){
        $this->title = null;
        $this->selected_id = null;
        $this->editionMode = false;
        $this->age = 18;

        $this->student=[
            'id_type' => null,
            'identification' => null,
            'name' => null,
            'lastname' => null,
            'identification' => null,
            'birthdate' => null,
            'main_phone' => null,
            'sec_phone' => null,
            'email' => null,
            'observations' => null,
        ];
        $this->attendant=[
            'id_type' => null,
            'identification' => null,
            'name' => null,
            'lastname' => null,
            'main_phone' => null,
            'email' => null,
        ];

        $this->interests_by_student = [];
        $this->id_type = IdType::all();
        $this->course_categories = CourseCategory::all();
        
    }

    public function render()
    {
        $data = Student::list()->orderby('id','DESC')->get();
        return view('livewire.students.index',compact('data'));
    }

    public function loadFields($record){
        $this->editionMode = true;
        $this->student = [
            'id' => $record->id,
            'id_type' => $record->id_type_id,
            'identification' => $record->identification,
            'name' => $record->name,
            'lastname' => $record->lastname,
            'birthdate' => $record->birthdate,
            'email' => $record->email,
            'main_phone' => $record->main_phone,
            'sec_phone' => $record->sec_phone
        ];
        
        
        if($this->editionMode && $this->age < 18) $this->attendant = [
            'id' => $record->attendant,
            'id_type' => $record->id_type_id_a,
            'identification' => $record->identification_a,
            'name' => $record->name_a,
            'lastname' => $record->lastname_a,
            'email' => $record->email_a,
            'main_phone' => $record->main_phone_a
        ];
        
        
        
        
        $interests = InterestCoursesStudent::where('student',$record->id)->get();
            $this->interests_by_student = [];
            foreach($interests as $item){
                array_push($this->interests_by_student,$item->course_category_id);
            }
        //$interests    

        
    }


    public function studentAge(){
        if($this->student != null){
            $birthdate = new DateTime($this->student['birthdate']);
            $now = new DateTime();
            $age = $now->diff($birthdate);
            $this->age = $age->y;
        }
        else{
            $this->age = 18;
        }
        
    }
    public function validateId(){
        if(isset($this->student['identification']) && $this->student['identification'] != null ){
            $ident = $this->student['identification'];
            $record = Student::details()->where('people.identification',$ident)->first();
            if($record){
                $this->loadFields($record);
            }
        }

    }

    public function save(){
        if(! $this->editionMode){
            $this->create();
        }
        else{
            $this->update();
        }
        $this->clearFields();
        $this->dispatchBrowserEvent('hideModal');
    }

    public function update(){
        $record = Person::find($this->student['id']);

        InterestCoursesStudent::where('student',$record['id'])->delete();
            foreach ($this->interests_by_student as $item) {
                InterestCoursesStudent::create([
                    'student' => $record['id'],
                    'course_category_id' => $item
                ]);
            }
        $person = [
            'id_type' => $this->student['id_type'],
            'name' => $this->student['name'],
            'lastname' => $this->student['lastname'],
            'birthdate' => $this->student['birthdate'],
            'email' => $this->student['email'],
            'main_phone' => $this->student['main_phone'],
            'sec_phone' => $this->student['sec_phone']
        ];
        
        $record->update($person);

        if($this->age <18 ){
            $record = Person::find($this->attendant['id']);
            $attendant = [
                'id_type' => $this->attendant['id_type'],
                'name' => $this->attendant['name'],
                'lastname' => $this->attendant['lastname'],
                'email' => $this->attendant['email'],
                'main_phone' => $this->attendant['main_phone']
            ];
            $record->update($attendant);

            

        }

      
    }

    public function loadAttendant(){
        $ident = $this->attendant['identification'];
        $record = Person::where('people.identification',$ident)->first();
        if($record){
            $this->attendant = [
                'id' => $record->id,
                'id_type' => $record->id_type,
                'identification' => $record->identification,
                'name' => $record->name,
                'lastname' => $record->lastname,
                'email' => $record->email,
                'main_phone' => $record->main_phone
            ];
        }   
    }
    public function create(){
        $this->title = "Registro de estudiante";
        $person = Person::create($this->student);
        if($this->age < 18){
            $record = Person::where('identification',$this->attendant['identification'])->first();
            if($record){
                
                $attendant = $record->update($this->attendant);
                $attendant_id = $record->id;
                
            }
            else{
                $attendant = Person::create($this->attendant);
                $attendant_id = $attendant->id;
            }
            
        }
        else{
            $attendant_id = $person->id;
        }
        $student = Student::create([
            'person' => $person->id,
            'enrolment_status' => 1,
            'attendant' => $attendant_id,
            'created_by' => Auth::user()->id
        ]);

        foreach ($this->interests_by_student as $item) {
            InterestCoursesStudent::create([
                'student' => $student->id,
                'course_category_id' => $item
            ]);
        }
        
    }

    public function formCreate(){
        $this->title = "Registro de estudiante";
        $this->editionMode = false;
        $this->age = 18;
        $this->clearFields();
        $this->dispatchBrowserEvent('showModal');
    }

    public function clearFields(){
        $this->student = null;
        $this->attendant= null;
        $this->interests_by_student = [];
    }

    public function selectItem($id,$mode){
        
        $this->selected_id = $id;
        $record = Student::details()->where('students.id',$id)->first();
        $this->interests_by_student = [];
        $this->loadFields($record);
        $this->studentAge();
        
        switch($mode){
            case 'edit': 
                $this->title = "Editar estudiante";
                $this->editionMode = true;
            break;
            case 'show': 
                $this->title = "Ver detalle";
                $this->EditionMode = false;
            break;
            
        }
        //$this->studentAge();
        $this->dispatchBrowserEvent('showModal');
            
    }
    
}
