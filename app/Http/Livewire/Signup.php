<?php

namespace App\Http\Livewire;

use Livewire\Component;

use App\Models\Student;
use App\Models\Person;
use App\Models\IdType;
use App\Models\InterestCoursesStudent;
use App\Models\CourseCategory;

use DateTime;

class Signup extends Component
{
    public $student, $attendant,$id_type, $course_categories, $interests_by_student;
    public $title, $selected_id, $editionMode, $age;
    
    public function mount(){
        $this->title = null;
        $this->selected_id = null;
        $this->editionMode = false;
        $this->age = 18;

        $this->student=[
            'id_type' => null,
            'identification' => null,
            'name' => null,
            'lastname' => null,
            'identification' => null,
            'birthdate' => null,
            'main_phone' => null,
            'sec_phone' => null,
            'email' => null,
            'observations' => null,
        ];
        $this->attendant=[
            'id_type' => null,
            'identification' => null,
            'name' => null,
            'lastname' => null,
            'main_phone' => null,
            'email' => null,
        ];

        $this->interests_by_student = [];
        $this->id_type = IdType::all();
        $this->course_categories = CourseCategory::all();
        
    }
    
    public function render()
    {
        return view('livewire.signup.index');
    }
}
