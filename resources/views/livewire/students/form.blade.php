<div  wire:ignore.self class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" id="form" >
    <div class="modal-lg modal-dialog">
        <!--Form-->
        <form method = "POST" wire:submit.prevent = "save">
        <div class="modal-content">
            
            <div class="modal-header">
            <h3 class="modal-title" id="exampleModalLabel">{{ $title }}</h3>
            <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            
            <div class="modal-body">
            <div class = "row">
                <div class="col-md-12"><h4>Datos personales</h4></div>
                <div class = "col-md-4">
                    <label for ="id_type">Tipo de documento</label>
                    <select id= "id_type" wire:model = "student.id_type" class= "form-control" required>
                        <option>Escoja una opción</option>
                        @foreach ($id_type as $item)
                        <option value = "{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class = "col-md-4">
                    <label for ="identification">Identificación</label>
                    <input type ="text" wire:model = "student.identification" id="identification" class= "form-control" required wire:focusout ="validateId">
                </div>

                <div class = "col-md-4">
                    <label for ="email">Correo electrónico</label>
                    <input type ="email" wire:model = "student.email" id="email" class = "form-control">
                </div>
                
                <div class = "col-md-3">
                    <label for ="name">Nombre</label>
                    <input type ="text" wire:model = "student.name" id="students" class= "form-control" required>
                </div>

                <div class = "col-md-3">
                    <label for ="lastname">Apellido</label>
                    <input type ="text" wire:model = "student.lastname" id="lastname" class = "form-control" required>
                </div>

                <div class = "col-md-3">
                    <label for ="birthdate">Fecha de nacimiento</label>
                    <input type ="date" wire:model = "student.birthdate" id="birthdate" class = "form-control" required wire:change="studentAge">
                </div>

                <div class = "col-md-3">
                <label for ="main_phone">Teléfono principal</label>
                <input type ="text" wire:model = "student.main_phone" id="main_phone" class = "form-control" required>
                </div>


                
                <div class = "col-md-12">
                    <div class="row">      
                        <div class = "col-md-3">
                            <label for ="sec_phone">Teléfono alternativo</label>
                            <input type ="text" wire:model = "student.sec_phone" id="sec_phone" class = "form-control">
                        </div>

                        <div class = "col-md-9">
                            <label for ="observations">Observaciones</label>
                            <textarea wire:model = "student.observations" id="observations" class = "form-control">
                            </textarea>
                        </div>
                    </div>

                </div>
                @if($age < 18)
                <div class="col-md-12"><h4>Datos del acudiente</h4></div>
                <div class = "col-md-4">
                    <label for ="id_type_a">Tipo de documento</label>
                    <select id= "id_type_a" wire:model = "attendant.id_type" class= "form-control" required>
                        <option>Escoja una opción</option>
                        @foreach ($id_type as $item)
                        <option value = "{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class = "col-md-4">
                    <label for ="a_identification">Identificación</label>
                    <input type ="text" wire:model = "attendant.identification" id="a_identification" class= "form-control" required wire:focusout="loadAttendant()">
                </div>

                <div class = "col-md-4">
                    <label for ="a_email">Correo electrónico</label>
                    <input type ="email" wire:model = "attendant.email" id="a_email" class = "form-control">
                </div>
                
                <div class = "col-md-4">
                    <label for ="name">Nombre</label>
                    <input type ="text" wire:model = "attendant.name" id="a_name" class= "form-control" required>
                </div>

                <div class = "col-md-4">
                    <label for ="lastname">Apellido</label>
                    <input type ="text" wire:model = "attendant.lastname" id="a_lastname" class = "form-control" required>
                </div>

                <div class = "col-md-4">
                <label for ="main_phone">Teléfono de contacto</label>
                <input type ="text" wire:model = "attendant.main_phone" id="a_main_phone" class = "form-control" required>
                </div>
                @endif

                <div class="col-md-12">
                    <h4>Cursos de interés</h4>
                </div>


                @foreach ($course_categories as $item)
                    <div class="col-md-3">
                        <input type = "checkbox" wire:model = "interests_by_student" id ="couse_id_{{$item->id}}" value="{{$item->id}}">{{ $item->name }}
                    </div>
                @endforeach
                
                @foreach ($interests_by_student as $item)
                    
                @endforeach
               

                
                
                
                
            </div>
            
        </div>
        <div class="modal-footer">
            <button type = "submit" class="btn btn-success">
            @if(! $editionMode)
                Registrar

            @else
                Guardar cambios
            @endif
            </button>   
        </div>




                

                
                
                

                
                
        </form><!-- End form -->   
    
    </div>
    
    </div>
</div><!--EndModal-->