<div class="py-2">
  <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 ">
      <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
        <button class="btn btn-primary" wire:click="formCreate">+ Nuevo estudiante</button>

        <!--Modal-->
        @include('livewire.students.form')
        <hr>
        <!--Students table-->
        <table class="table table-bordered">
          <thead class ="thead thead-dark">
              <tr>
                  
                  <th class="px-4 py-2">Nombre completo</th>
                  <th class="px-4 py-2">Teléfonos</th>
                  <th class="px-4 py-2">Estado</th>
                  <th class="px-4 py-2">Acciones</th>
              </tr>
          </thead>
          <tbody>
              @forelse ($data as $item)
                  <tr>
                      
                      <td>{{ $item->name }} {{ $item->lastname}}</td>
                      <td>
                      @if($item->main_phone != null)
                      {{ $item->main_phone}}
                      @endif
                      @if( $item->sec_phone != null) 
                      - {{ $item->sec_phone }}
                      @endif

                      </td>
                      
                      <td>{{ $item->status }}</td>
                      <td>
                      <button wire:click="selectItem({{ $item->id }}, 'edit' )" 
                        class="btn btn-primary material-icons" title = "Editar">
                        create
                      </button>
                      <button wire:click="selectItem({{ $item->id }}, 'show' )" 
                        class="btn btn-success material-icons" title="Ver detalles">
                        visibility
                      </button>
                      </td>
                  </tr>
              @empty
                  <tr class="text-center">
                      <td colspan="4" class="py-3 italic">No hay datos registrados</td>
                  </tr>
              @endforelse
          </tbody>
      </table><!--End students table-->
      <script>
        window.addEventListener('hideModal', event => {
          $('#form').modal('hide');
        })

        window.addEventListener('showModal', event => {
          $('#form').modal('show');
        })


      </script>
      
      </div>
    </div>
  </div>
<div>


