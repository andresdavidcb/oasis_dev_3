<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\EnrolmentStatus;
class enrolmentStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = ['Inscrito','Matriculado','Inactivo','Retirado'];
        foreach ($data as $item) {
            EnrolmentStatus::create(['name'=> $item]);
        }
    }
}
