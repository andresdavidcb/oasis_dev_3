<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\IdType;

class IdTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name' => 'Tarjeta de identidad','abrev' =>'TI'],
            ['name' => 'Cédula de ciudadanía','abrev' =>'CC'],
            ['name' => 'Pasaporte','abrev' =>'PP'],
            ['name' => 'Cédula de extranjería','abrev' =>'CE'],
        ];
    
        foreach ($data as $item) {
            IdType::create($item);
        }
        
    }
}
