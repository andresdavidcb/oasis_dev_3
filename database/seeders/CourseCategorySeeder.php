<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\CourseCategory;

class CourseCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = ['Piano','Guitarra','Vocalización','Batería','Saxofón','Bajo','Violín','Chelo','Acordión'];
        foreach ($data as $item) {
            CourseCategory::create(['name'=> $item]);
        }
    }
}
